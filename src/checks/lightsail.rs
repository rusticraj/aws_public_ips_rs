use aws_config::SdkConfig;
use aws_sdk_lightsail as lightsail;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = lightsail::Client::new(config);

    let mut result = vec![];

    let output = client.get_instances().send().await?;
    for instance in output.instances() {
        result.push(Output {
            id: instance.name.clone(),
            hostname: None,
            ip_addresses: vec![instance.public_ip_address.clone().unwrap()],
        });
    }

    let output = client.get_load_balancers().send().await?;
    for load_balancer in output.load_balancers() {
        let hostname = load_balancer.dns_name.clone();
        result.push(Output {
            id: load_balancer.name.clone(),
            hostname: hostname.clone(),
            ip_addresses: crate::utils::resolve_hostname(hostname.unwrap().as_str()),
        });
    }

    Ok(result)
}
