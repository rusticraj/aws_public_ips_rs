use aws_config::SdkConfig;
use aws_sdk_rds as rds;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = rds::Client::new(config);

    let output = client.describe_db_instances().send().await?;

    Ok(output
        .db_instances()
        .into_iter()
        .filter_map(|instance| {
            if !instance.publicly_accessible.unwrap() {
                return None;
            }

            if instance.endpoint.is_none() {
                panic!("RDS has nil endpoint")
            }

            let hostname = instance.endpoint().unwrap().address.clone();
            Some(Output {
                id: instance.dbi_resource_id.clone(),
                hostname: hostname.clone(),
                ip_addresses: crate::utils::resolve_hostname(hostname.unwrap().as_str()),
            })
        })
        .collect())
}
