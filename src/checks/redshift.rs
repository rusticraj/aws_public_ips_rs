use aws_config::SdkConfig;
use aws_sdk_redshift as redshift;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = redshift::Client::new(config);

    let output = client.describe_clusters().send().await?;

    Ok(output
        .clusters()
        .iter()
        .filter_map(|cluster| {
            if !cluster.publicly_accessible.unwrap() {
                return None;
            }

            if cluster.endpoint.is_none() {
                panic!("Redshift has nil endpoint")
            }

            let hostname = cluster.endpoint().unwrap().address.clone();
            Some(Output {
                id: cluster.cluster_identifier.clone(),
                hostname: hostname.clone(),
                ip_addresses: crate::utils::resolve_hostname(hostname.unwrap().as_str()),
            })
        })
        .collect())
}
