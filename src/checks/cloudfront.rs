use aws_config::SdkConfig;
use aws_sdk_cloudfront as cloudfront;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = cloudfront::Client::new(config);
    let output = client.list_distributions().send().await?;

    Ok(output
        .distribution_list()
        .map(|x| x.items())
        .unwrap_or_default()
        .iter()
        .map(|distribution| Output {
            id: Some(distribution.id.clone()),
            hostname: Some(distribution.domain_name.clone()),
            ip_addresses: crate::utils::resolve_hostname(distribution.domain_name()),
        })
        .collect())
}
