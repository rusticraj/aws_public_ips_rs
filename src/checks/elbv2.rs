use aws_config::SdkConfig;
use aws_sdk_elasticloadbalancingv2 as elbv2;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = elbv2::Client::new(config);
    let output = client.describe_load_balancers().send().await?;

    Ok(output
        .load_balancers()
        .iter()
        .filter_map(|load_balancer| {
            if load_balancer.scheme == Some(elbv2::types::LoadBalancerSchemeEnum::InternetFacing) {
                return None;
            }
            Some(Output {
                id: load_balancer.canonical_hosted_zone_id.clone(),
                hostname: load_balancer.dns_name.clone(),
                ip_addresses: crate::utils::resolve_hostname(load_balancer.dns_name().unwrap()),
            })
        })
        .collect())
}
