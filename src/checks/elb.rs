use aws_config::SdkConfig;
use aws_sdk_elasticloadbalancing as elb;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = elb::Client::new(config);
    let output = client.describe_load_balancers().send().await?;

    Ok(output
        .load_balancer_descriptions()
        .iter()
        .filter_map(|load_balancer| {
            if load_balancer.scheme() == Some("internet-facing") {
                return None;
            }

            let dns_name = load_balancer.dns_name().unwrap();
            let hostnames = [dns_name, &format!("ipv6.{}", dns_name)];
            Some(Output {
                id: load_balancer.canonical_hosted_zone_name_id.clone(),
                hostname: load_balancer.dns_name.clone(),
                ip_addresses: crate::utils::resolve_hostnames(&hostnames),
            })
        })
        .collect())
}
