use aws_config::SdkConfig;
use aws_sdk_apigateway as apigateway;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = apigateway::Client::new(config);
    let output = client.get_rest_apis().send().await?;

    Ok(output
        .items()
        .iter()
        .map(|api| {
            let hostname = format!(
                "{}.execute-api.{}.amazonaws.com",
                api.id().unwrap(),
                client.config().region().unwrap()
            );
            Output {
                id: api.id.clone(),
                hostname: Some(hostname.clone()),
                ip_addresses: crate::utils::resolve_hostname(&hostname),
            }
        })
        .collect())
}
