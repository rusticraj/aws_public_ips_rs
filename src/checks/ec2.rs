use aws_config::SdkConfig;
use aws_sdk_ec2 as ec2;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = ec2::Client::new(config);
    let output = client.describe_instances().send().await?;

    let mut results = vec![];
    for reservation in output.reservations() {
        for instance in reservation.instances() {
            let mut ip_addresses = vec![];
            if let Some(public_ip) = instance.public_ip_address.clone() {
                ip_addresses.push(public_ip);
            }
            for interface in instance.network_interfaces() {
                let mut public_ips = vec![];

                for private_ip in interface.private_ip_addresses() {
                    if let Some(assoc) = private_ip.association() {
                        if let Some(public_ip) = assoc.public_ip() {
                            public_ips.push(public_ip.to_string());
                        }
                    }
                }
                public_ips.extend(
                    interface
                        .ipv6_addresses()
                        .iter()
                        .filter_map(|x| x.ipv6_address.clone()),
                );

                ip_addresses.extend(public_ips);
            }

            // Don't add to results if all ips were private
            if ip_addresses.is_empty() {
                continue;
            }

            // sort IP addresses and remove duplicates
            ip_addresses.sort();
            ip_addresses.dedup();

            results.push(Output {
                id: instance.instance_id.clone(),
                hostname: instance.public_dns_name.clone(),
                ip_addresses,
            });
        }
    }

    Ok(results)
}
