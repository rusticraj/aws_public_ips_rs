use aws_config::SdkConfig;
use aws_sdk_elasticsearch as elasticsearch;

use crate::checks::Output;

pub async fn run(config: &SdkConfig) -> Result<Vec<Output>, super::ErrType> {
    let client = elasticsearch::Client::new(config);
    let output = client.list_domain_names().send().await?;

    let mut results = vec![];
    for domain_name in output.domain_names() {
        let domain = client
            .describe_elasticsearch_domain()
            .domain_name(domain_name.domain_name().unwrap_or_default())
            .send()
            .await?;

        let hostname = domain.domain_status().unwrap().endpoint();

        if let Some(hostname) = hostname {
            results.push(Output {
                id: Some(domain.domain_status().unwrap().domain_id.clone()),
                hostname: Some(hostname.to_string()),
                ip_addresses: crate::utils::resolve_hostname(hostname),
            });
        }
    }

    Ok(results)
}
