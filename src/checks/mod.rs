use std::{error::Error, sync::Arc};

use aws_config::SdkConfig;

use clap::ValueEnum;

mod apigateway;
mod cloudfront;
mod ec2;
mod elasticsearch;
mod elb;
mod elbv2;
mod lightsail;
mod rds;
mod redshift;

type ErrType = Box<dyn Error + Send + Sync>;

#[derive(Clone, ValueEnum, Debug, Hash)]
#[value(rename_all = "lower")]
pub enum Service {
    APIGateway,
    CloudFront,
    EC2,
    ElasticSearch,
    ELB,
    ELBv2,
    Lightsail,
    RDS,
    Redshift,
}

#[derive(Debug)]
pub struct Output {
    pub id: Option<String>,
    pub hostname: Option<String>,
    pub ip_addresses: Vec<String>,
}

pub async fn run_service(
    service: Service,
    config: Arc<SdkConfig>,
) -> (Service, Result<Vec<Output>, ErrType>) {
    use Service::*;
    (
        service.clone(),
        match service {
            APIGateway => apigateway::run(&config).await,
            CloudFront => cloudfront::run(&config).await,
            EC2 => ec2::run(&config).await,
            ELB => elb::run(&config).await,
            ELBv2 => elbv2::run(&config).await,
            ElasticSearch => elasticsearch::run(&config).await,
            Lightsail => lightsail::run(&config).await,
            Redshift => redshift::run(&config).await,
            RDS => rds::run(&config).await,
        },
    )
}
