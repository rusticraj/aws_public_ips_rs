mod checks;
mod utils;

use crate::checks::Service;

use std::error::Error;
use std::sync::Arc;

use aws_config::{self, BehaviorVersion};
use clap::{Parser, ValueEnum};
use futures::stream::{FuturesUnordered, StreamExt};
use tokio::task::JoinHandle;

#[derive(Parser, Debug)]
#[command(version, about, long_about=None)]
struct Args {
    #[arg(
        short,
        long,
        value_delimiter = ',',
        help = "List of AWS Services to check"
    )]
    services: Vec<Service>,
    // #[arg(short, long, help="Set output format")]
    // formatter: Formatter
    #[arg(short, long, action=clap::ArgAction::SetTrue)]
    verbose: bool,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    let config = Arc::new(aws_config::load_defaults(BehaviorVersion::latest()).await);

    let mut tasks = args
        .services
        .into_iter()
        .map(|service| tokio::spawn(checks::run_service(service, config.clone())))
        .collect::<FuturesUnordered<JoinHandle<_>>>();

    while let Some(result) = tasks.next().await {
        let (service, result) = result?;
        let service_name = service.to_possible_value().unwrap().get_name().to_string();
        match result {
            Ok(outputs) => {
                println!("Service: {}", service_name);

                for output in outputs {
                    println!("{:#?}", output);
                }
                println!();
            }
            Err(err) => {
                println!("WARN: Check for {} failed: {}", service_name, err);
            }
        };
    }

    Ok(())
}
