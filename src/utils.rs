use dns_lookup::lookup_host;

pub fn has_service() -> bool {
    unimplemented!()
}

pub fn resolve_hostname(hostname: &str) -> Vec<String> {
    lookup_host(hostname)
        .unwrap_or_default()
        .into_iter()
        .map(|ip_addr| ip_addr.to_string())
        .collect()
}

pub fn resolve_hostnames(hostnames: &[&str]) -> Vec<String> {
    hostnames
        .iter()
        .map(|hostname| resolve_hostname(hostname))
        .flatten()
        .collect()
}
